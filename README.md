
## Fake VS Real News

A model used to predict the truthfulness of an article given labeled datasets. Built with Jupyter Notebook.

## Project Screen Shot(s)

##### Word Cloud from TFIDF vectors of News texts
![Screen_Shot_2021-07-12_at_11.54.57_PM](/uploads/3f7c237cc81940064df3b4a2a607bef7/Screen_Shot_2021-07-12_at_11.54.57_PM.png)

##### Results
![Screen_Shot_2021-07-17_at_12.35.09_PM](/uploads/a321611eecb43b41c4341ad88eaf41fd/Screen_Shot_2021-07-17_at_12.35.09_PM)
## Installation and Setup Instructions


## Reflection

This project took about a week to get the current results. Project goals included reviewing the quality of the dataset, extracting information from the dataset, and solving the problem of classifying whether the given news is fake or real.

I chose to primarily use the linear SVM model because it is a common classifier algorithm. However, I was surprised to see the accuracies that my models were producing. With further analysis, I discovered that the fake and the real news used for training and testing were pulled from different sources, thus had a very different style of writing which made it easier for my model to accurately predict the realness of the article. I tried to mitigate this bias by omitting the top 5 TF-IDF features (both positive and negative) from the model but I believe it is not enough to fully make the model generalizable to any articles out in the world. This "leakage" in the data allowed for my model to quickly pick up on what makes a certain article fake or real in this specific dataset. 

Although there were some biases in the dataset it was challenging, and therefore a really good learning experience, to extract meaningful features from a given dataset, mainly in the form of texts in this project. By implementing TFIDF vectors, I was able to experience what modifying datasets are like and use them to achieve my target variable of real or fake. 

My next step would be to make this model generalizable and not overfitting to the specific way in which the labeled datasets were collected. 
